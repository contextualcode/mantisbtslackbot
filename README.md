# MantisBT Slackbot

## Introduction

MantisBT Slackbot will unfurl links from MantisBT.

## Requirements

- node
- npm

## Setup/Config

1. You need a `.env` file with the following variables:

- `MANTIS_SLACKBOT_CLIENT_ID`
- `MANTIS_SLACKBOT_CLIENT_SECRET`
- `MANTIS_SLACKBOT_VERIFICATION_TOKEN`
- `MANTIS_SLACKBOT_OAUTH_TOKEN`
- `MANTIS_SLACKBOT_USERNAME`
- `MANTIS_SLACKBOT_PASSWORD`

The account you use for the Mantis login must have at least Reporter permissions on the projects you want unfurling to work on. If you want to unfurl private notes, make sure to adjust the permissions appropriately (to Developer, for example).

2. Copy `config.js.sample` to `config.js`, and adjust it for your needs. At a minimum, you must change `config.mantis.base_url` to point to your MantisBT install.

## Usage

Run `npm install`, and then `node index.js`.

To run the script forever, you can use [forever](https://www.npmjs.com/package/forever): `forever start index.js`.

You must adjust your Slack app settings. Add the base URL of your newly running node app to "Redirect URLs". Then under "Event Subscriptions", enable events and add `$YOUR_DOMAIN/event` as the Request URL, where `$YOUR_DOMAIN` is the URL to your node app. Subscribe to the "link_shared" event. And add your MantisBT domain to the "App Unfurl Domains".

In `config.js`, you can change `config.slack.unfurl_style` to `"long"`, `"medium"`, or `"short"` to get different amounts of info in the unfurl.
