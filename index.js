/*jslint node: true */
"use strict";

const config = require("./config");
const dotenv = require("dotenv").config();
const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const soap = require("soap");
const urlPackage = require("url");

let app = express();

app.use(bodyParser.json());

app.listen(config.app.port);

console.log("Mantis Slackbot running");

app.get("/oauth", function(req, res) {
    if (!req.query.code) {
        res.status(500);
        res.send({"Error": "Looks like we're not getting code."});
        console.log("Looks like we're not getting code.");
    }
    else {
        request(
            {
                url: config.slack.api_url + "/oauth.access",
                qs: {
                    code: req.query.code,
                    client_id: process.env.MANTIS_SLACKBOT_CLIENT_ID,
                    client_secret: process.env.MANTIS_SLACKBOT_CLIENT_SECRET
                },
                method: "GET"
            },
            function (error, response, body) {
                if (error) {
                    console.log(error);
                }
                else {
                    res.json(body);
                }
            }
        );
    }
});

function getChatUnfurl(issueInfo, callback) {
    let unfurlText = "";
    const includeDescription = [
        "long",
        "medium"
    ];
    if (includeDescription.includes(config.slack.unfurl_style)) {
        unfurlText += issueInfo.description;
    }

    const includeInfo = [
        "long"
    ];
    let fields = [];
    if (includeInfo.includes(config.slack.unfurl_style)) {
        fields = [
            {
                "title": "Reporter, Severity",
                "value": issueInfo.reporter + ", " + issueInfo.severity,
                "short": true
            },
            {
                "title": "Status, Assigned To",
                "value": issueInfo.status + ", " + issueInfo.handler,
                "short": true
            }
        ];
    }

    if (issueInfo.hasOwnProperty("note")) {
        unfurlText = "";
        fields.unshift(
            {
                "title": "Note by " + issueInfo.note.reporter,
                "value": issueInfo.note.text,
                "short": false
            }
        );
    }

    const unfurl = {
        "fallback": issueInfo.url,
        "title": "[" + issueInfo.project + "] " + issueInfo.title,
        "title_link": issueInfo.url,
        "text": unfurlText,
        "color": issueInfo.color,
        "fields": fields,
        "footer": config.slack.footer_name,
        "footer_icon": config.slack.footer_icon
    };

    callback(unfurl);
}

function sendChatUnfurl(event, unfurls) {
    let unfurlParams = "token=";
    unfurlParams += encodeURIComponent(process.env.MANTIS_SLACKBOT_OAUTH_TOKEN);
    unfurlParams += "&channel=" + encodeURIComponent(event.channel);
    unfurlParams += "&ts=" + encodeURIComponent(event.message_ts);
    unfurlParams += "&unfurls=";
    unfurlParams += encodeURIComponent(JSON.stringify(unfurls));
    request.post(
        config.slack.api_url + "/chat.unfurl?" + unfurlParams
    );
}

function getIssueInfo(url, callback) {
    let urlObj = urlPackage.parse(url, true);
    // in query like example.com/view.php?id=12345
    let issueID = urlObj.query.id;
    if (!issueID) {
        // in path like example.com/12345
        issueID = urlObj.pathname.split("/");
        if (issueID.length < 2) {
            callback();
            return;
        }
        issueID = parseInt(issueID[1]);
        if (Number.isNaN(issueID)) {
            callback();
            return;
        }
    }

    // check if a note is linked
    let noteID = urlObj.hash;
    if (noteID) {
        noteID = noteID.split("#c");
        noteID = noteID.length < 2 ? null : parseInt(noteID[1]);
    }

    const getIssueCallback = function(err, result, raw, soapHeader) {
        if (!result || !result.return) {
            callback();
            return;
        }
        let data = result.return;

        let issueInfo = {};
        issueInfo.url = url;
        issueInfo.title = data.summary.$value;

        const props = [
            "description"
        ];

        props.forEach(function(prop, i) {
            issueInfo[prop] = "";
            if (
              data.hasOwnProperty(prop) &&
              data[prop].hasOwnProperty("$value")
            ) {
                issueInfo[prop] = data[prop].$value;
            }
        });

        if (
          issueInfo.hasOwnProperty("description") &&
          issueInfo.description.length > 140
        ) {
            issueInfo.description = issueInfo.description.substring(0, 140);
            issueInfo.description += "...";
        }

        const propsWithName = [
            "project",
            "status",
            "severity",
            "reporter",
            "handler"
        ];

        propsWithName.forEach(function(prop, i) {
            issueInfo[prop] = "";
            if (
              data.hasOwnProperty(prop) &&
              data[prop].hasOwnProperty("name") &&
              data[prop].name.hasOwnProperty("$value")
            ) {
                issueInfo[prop] = data[prop].name.$value;
            }
        });

        issueInfo.color = "";
        if (
          issueInfo.status !== "" &&
          issueInfo.status in config.mantis.colors
        ) {
            issueInfo.color = config.mantis.colors[issueInfo.status];
        }

        // grab note info
        if (noteID) {
            let notes = data.notes.item;
            if (!Array.isArray(notes)) {
                notes = [notes];
            }
            notes.forEach(function(note) {
                if (note.id.$value === noteID) {
                    let noteText = note.text.$value;
                    if (noteText.length > 140) {
                        noteText = noteText.substring(0, 140) + "...";
                    }
                    issueInfo.note = {
                        "text":     noteText,
                        "reporter": note.reporter.name.$value
                    };
                }
            });
        }

        callback(issueInfo);
    };

    const soapCallback = function(err, client) {
        client.mc_issue_get(
            {
                "username": process.env.MANTIS_SLACKBOT_USERNAME,
                "password": process.env.MANTIS_SLACKBOT_PASSWORD,
                "issue_id": issueID
            },
            getIssueCallback
        );
    };
    const wsdl = config.mantis.base_url + config.mantis.wsdl_path;
    soap.createClient(wsdl, soapCallback);
}

app.post("/event", function(req, res) {
    if (req.body.type === "url_verification") {
        res.send(req.body.challenge);
        return;
    }

    if (req.body.token !== process.env.MANTIS_SLACKBOT_VERIFICATION_TOKEN) {
        res.sendStatus(401, "Invalid verification token.");
        return;
    }

    let event = req.body.event;

    if (event.type !== "link_shared") {
        res.sendStatus(500);
        return;
    }

    res.sendStatus(200);

    let links = event.links;
    let unfurls = {};
    let numProcessed = 0;

    links.forEach(function(link, i) {
        getIssueInfo(link.url, function(issueInfo) {
            if (!issueInfo) {
                return;
            }

            getChatUnfurl(issueInfo, function(unfurl) {
                numProcessed += 1;

                unfurls[issueInfo.url] = unfurl;

                if (numProcessed === links.length) {
                    sendChatUnfurl(event, unfurls);
                }
            });
        });
    });
});
